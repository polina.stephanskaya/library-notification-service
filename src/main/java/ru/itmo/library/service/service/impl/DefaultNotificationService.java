package ru.itmo.library.service.service.impl;

import org.springframework.stereotype.Service;
import ru.itmo.library.service.model.ReturnNotificationEvent;
import ru.itmo.library.service.service.api.NotificationService;

@Service
public class DefaultNotificationService implements NotificationService {

    @Override
    public void sendNotification(ReturnNotificationEvent event) {
        System.out.println("Сообщение отправлено получателю:");
        System.out.println("Уважаемый " + event.getClientName() + "!\n"
                + "Напоминаем Вам о приближении срока возврата книги " + event.getBook() + ".\n"
                + "Срок аренды до " + event.getReturnDate());
    }

}
