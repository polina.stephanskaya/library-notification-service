package ru.itmo.library.service.service.api;

import ru.itmo.library.service.model.ReturnNotificationEvent;

public interface NotificationService {

    void sendNotification(ReturnNotificationEvent event);

}
