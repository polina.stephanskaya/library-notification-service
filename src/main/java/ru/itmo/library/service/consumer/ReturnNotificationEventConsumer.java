package ru.itmo.library.service.consumer;

import lombok.RequiredArgsConstructor;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import ru.itmo.library.service.model.ReturnNotificationEvent;
import ru.itmo.library.service.service.api.NotificationService;

@Component
@RequiredArgsConstructor
public class ReturnNotificationEventConsumer {

    private final NotificationService service;

    @KafkaListener(topics = "${spring.kafka.template.default-topic}", groupId = "${spring.kafka.consumer.group-id}")
    public void listen(ReturnNotificationEvent event) {
        System.out.println("Received Message: " + event);

        service.sendNotification(event);
    }

}
