package ru.itmo.library.service.config;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaAdmin;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaTopicConfig {

    @Bean
    @Profile("LOCAL")
    public KafkaAdmin admin(@Value("${spring.kafka.bootstrap.servers}") String bootstrapServers) {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        return new KafkaAdmin(configs);
    }

    @Bean
    @Profile("YANDEX")
    public KafkaAdmin adminYandex(@Value("${spring.kafka.bootstrap.servers}") String bootstrapServers,
                                  @Value("${spring.kafka.username}") String username,
                                  @Value("${spring.kafka.password}") String password,
                                  @Value("${spring.kafka.sasl.mechanism}") String saslMechanism,
                                  @Value("${spring.kafka.login.module}") String loginModule) {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        configs.putAll(securityConfigs(saslMechanism, loginModule, username, password));
        return new KafkaAdmin(configs);
    }

    public static Map<String, Object> securityConfigs(String saslMechanism,
                                                      String loginModule,
                                                      String username,
                                                      String password) {
        Map<String, Object> props = new HashMap<>();
        String jaasTemplate = "%s required username=\"%s\" password=\"%s\";";
        String jaasCfg = String.format(jaasTemplate, loginModule, username, password);
        props.put("security.protocol", "SASL_PLAINTEXT");
        props.put("sasl.mechanism", saslMechanism);
        props.put("sasl.jaas.config", jaasCfg);
        return props;
    }

    @Bean
    public NewTopic topic(@Value("${spring.kafka.template.default-topic}") String topic) {
        return new NewTopic(topic, 1, (short) 1);
    }

}
