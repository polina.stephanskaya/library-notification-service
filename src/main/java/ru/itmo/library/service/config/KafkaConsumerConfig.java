package ru.itmo.library.service.config;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import ru.itmo.library.service.model.ReturnNotificationEvent;

import java.util.HashMap;
import java.util.Map;

@EnableKafka
@Configuration
public class KafkaConsumerConfig {

    @Value("${spring.kafka.bootstrap.servers}")
    private String bootstrapServers;
    @Value("${spring.kafka.consumer.group-id}")
    private String groupId;

    @Bean
    @Profile("LOCAL")
    public ConsumerFactory<String, ReturnNotificationEvent> consumerFactoryLocal() {
        Map<String, Object> props = new HashMap<>();
        props.put(
                ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
                bootstrapServers);
        props.put(
                ConsumerConfig.GROUP_ID_CONFIG,
                groupId);
        return new DefaultKafkaConsumerFactory<>(props,
                new StringDeserializer(),
                new JsonDeserializer<>(ReturnNotificationEvent.class));
    }

    @Bean
    @Profile("YANDEX")
    public ConsumerFactory<String, ReturnNotificationEvent> consumerFactoryYandex(@Value("${spring.kafka.username}") String username,
                                                                                  @Value("${spring.kafka.password}") String password,
                                                                                  @Value("${spring.kafka.sasl.mechanism}") String saslMechanism,
                                                                                  @Value("${spring.kafka.login.module}") String loginModule) {
        Map<String, Object> props = new HashMap<>();
        props.put(
                ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
                bootstrapServers);
        props.put(
                ConsumerConfig.GROUP_ID_CONFIG,
                groupId);
        props.putAll(KafkaTopicConfig.securityConfigs(saslMechanism, loginModule, username, password));
        return new DefaultKafkaConsumerFactory<>(props,
                new StringDeserializer(),
                new JsonDeserializer<>(ReturnNotificationEvent.class));
    }


    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, ReturnNotificationEvent>
    kafkaListenerContainerFactory(ConsumerFactory<String, ReturnNotificationEvent> consumerFactory) {

        ConcurrentKafkaListenerContainerFactory<String, ReturnNotificationEvent> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory);
        return factory;
    }

}
