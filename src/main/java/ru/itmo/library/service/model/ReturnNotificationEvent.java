package ru.itmo.library.service.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReturnNotificationEvent {

    private UUID id;
    private String phoneNumber;
    private String book;
    private String clientName;
    private LocalDate returnDate;
    private int attempts;
    private EventState state;
    private String lastErrorMessage;
    private LocalDate modifiedDate;

}
