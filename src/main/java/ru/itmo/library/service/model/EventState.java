package ru.itmo.library.service.model;

public enum EventState {
    CREATED,
    COMPLETED,
    FAILED
}
