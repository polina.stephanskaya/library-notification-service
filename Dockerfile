FROM maven:3.8.1-openjdk-17-slim AS build

WORKDIR /build

RUN mkdir -p /root/.m2 \
      && mkdir /root/.m2/repository

COPY pom.xml .
RUN mvn dependency:go-offline

COPY src src
RUN mvn package -Dmaven.test.skip=true

FROM openjdk:11
COPY --from=build /build/target/library-notification-service-1.0.jar /app/target/library-notification-service-1.0.jar

ENTRYPOINT [ "java", "-jar", "/app/target/library-notification-service-1.0.jar"]